export const Header: React.FC = () => {
    return (
        <div className="head">
            <div className="icon cloudy"></div>
            <div className="current-date">
                <p>суббота</p>
                <span>3 апреля</span>
            </div>
        </div>
    );
};
