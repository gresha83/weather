export const Day: React.FC<DayProps> = (props) => {
    const weekdays = new Array(7);
    weekdays[0] = "Воскресенье";
    weekdays[1] = "Понедельник";
    weekdays[2] = "Вторник";
    weekdays[3] = "Среда";
    weekdays[4] = "Четверг";
    weekdays[5] = "Пятниуа";
    weekdays[6] = "Суббота";

    const weekDay = weekdays[(new Date(props.day)).getDay()]
    console.log(props.isSelected, props.isSelected ? 'selected' :'t');
    return (
        <div className={'day ' + props.type + (props.isSelected ? ' selected' :'')}>
            <p>{weekDay}</p>
            <span>{props.temperature}</span>
        </div>
    );
};

/* Types */
interface DayProps {
    day: number;
    isSelected: boolean;
    temperature: number;
    type: string
}