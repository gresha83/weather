import { Day } from './Day';
import days from '../data/forecast.json';

export const Forecast: React.FC = () => {
    const daysInWeek = 7;
    const daysJSX = days.slice(0, daysInWeek).map(day => {
        return <Day key={day.id} temperature={day.temperature} day={day.day} type={day.type} isSelected={false} />;
    });

    return (
        <div className="forecast">
            {daysJSX}
        </div>
    );
};
