/* Components */
import { Filter, Header, CurrentWeather, Forecast } from './components';

export const App: React.FC = () => {
    return (
        <main>
            <Filter />
            <Header />
            <CurrentWeather />
            <Forecast />
        </main>
    );
};
